#include <fstream>
#include <iostream>
#include <stdio.h>
#include "image_ppm.h"

// visualiser les contours à partir d'une image seuillée et d'une image dilatée
void difference(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 4)
  {
    printf("Utilisation : ImgLue1.pgm ImgLu2.pgm ImgEcrite.pgm \n");
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue1);
  sscanf (argv[2],"%s",cNomImgLue2);
  sscanf (argv[3],"%s",cNomImgEcrite);
  OCTET *ImgLue1, *ImgLue2, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue2, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue1, OCTET, nTaille);
  lire_image_pgm(cNomImgLue1, ImgLue1, nH * nW);
  allocation_tableau(ImgLue2, OCTET, nTaille);
  lire_image_pgm(cNomImgLue2, ImgLue2, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  for (int i = 0; i < nTaille; i++)
  {
    if (ImgLue1[i] == 0 && ImgLue2[i] == 0)
	  {
	    ImgEcrite[i] = 255;
	  }
    else if (ImgLue1[i] == 255 && ImgLue2[i] == 255)
	  {
	    ImgEcrite[i] = 255;
	  }
    else ImgEcrite[i] = 0;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue1);
  free(ImgLue2);
  free(ImgEcrite);
}

// boucher les petits trous isolés (blancs) des objets
void dilatation(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm \n");
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel_min;
  for (int i = 0; i < nH; i++)
  {      
    for (int j = 0; j < nW; j++)
	  {
      pixel_min = ImgLue[i * nW + j];
      for (int k = i - 1; k < i + 2; k++)
	    {
        for (int l = j - 1; l < j + 2; l++)
		    {
          if (k >= 0 && k < nH && l >= 0 && l < nW && ImgLue[k * nW + l] < pixel_min)
		      {
            pixel_min = ImgLue[k * nW + l];
          }
        }
      }
      ImgEcrite[i * nW + j] = pixel_min;
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// EQM
float erreur_quadratique_moyenne(int argc, char* argv[])
{
  char cNomImgLuePPM[250], cNomImgLuePGM[250];
  int nH, nW, nTaille, nR, nG, nB;
  if (argc != 3)
  {
    printf("Utilisation: ImgLuePPM.ppm ImgLuePGM.pgm \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLuePPM) ;
  sscanf (argv[2],"%s",cNomImgLuePGM);
  OCTET *ImgLuePPM, *ImgLuePGM;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLuePPM, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLuePPM, OCTET, nTaille3);
  lire_image_ppm(cNomImgLuePPM, ImgLuePPM, nH * nW);
  allocation_tableau(ImgLuePGM, OCTET, nTaille);
  lire_image_pgm(cNomImgLuePGM, ImgLuePGM, nH * nW);

  int pixel_gris, pixel_rouge, pixel_vert, pixel_bleu, difference;
  pixel_gris = pixel_rouge = pixel_vert = pixel_bleu = difference = 0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel_rouge = ImgLuePPM[i * 3];
    pixel_vert = ImgLuePPM[i * 3 + 1];
    pixel_bleu = ImgLuePPM[i * 3 + 2];
    pixel_gris = ImgLuePGM[i];
    difference += (pixel_rouge - pixel_gris) * (pixel_rouge - pixel_gris);
    difference += (pixel_vert - pixel_gris) * (pixel_vert - pixel_gris);
    difference += (pixel_bleu - pixel_gris) * (pixel_bleu - pixel_gris);
  }
  return difference / nTaille3;
}

// supprimer les points objets (noirs) isolés
void erosion(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm \n");
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel_max;
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
	  {
      pixel_max = ImgLue[i * nW + j];
      for (int k = i - 1; k < i + 2; k++)
	    {
        for (int l = j - 1; l < j + 2; l++)
		    {
          if (k >= 0 && k < nH && l >= 0 && l < nW && ImgLue[k * nW + l] > pixel_max)
		      {
            pixel_max = ImgLue[k * nW + l];
          }
        }
      }
      ImgEcrite[i * nW + j] = pixel_max;
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// enchainer une dilatation et une érosion (boucher des trous dans les objets)
void fermeture(int argc, char* argv[])
{
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm \n");
    exit(1);
  }

  char cNomImgLue[250], cNomImgEcrite[250];
  char cNomTemp[250] = "temporaire.pgm";
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  argv[2] = cNomTemp;
  dilatation(argc, argv);
  argv[1] = cNomTemp;
  argv[2] = cNomImgEcrite;
  erosion(argc, argv);
  argv[1] = cNomImgLue;

  remove(cNomTemp);
}

// remplacer la valeur d'un pixel par la valeur moyenne de ce pixel avec ses 4 voisins
void filtre_flou_1(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm\n"); 
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel;
  for (int i = 1; i < nH - 1; i++)
  {
    for (int j = 1; j < nW - 1; j++)
	  {
      pixel = ImgLue[i * nW + j] + ImgLue[(i - 1) * nW + j] + ImgLue[(i + 1) * nW + j] + ImgLue[i * nW + j - 1] + ImgLue[i * nW + j + 1];
      pixel /= 5;
      ImgEcrite[i * nW + j] = pixel;
	  }
  }
  for (int i = 0; i < nH; i += (nH - 1))
  {
    for (int j = 0; j < nW; j++)
    {
      ImgEcrite[i * nW + j] = ImgLue[i * nW + j];
    }
  }
  for (int j = 0; j < nW; j += (nW - 1))
  {
    for (int i = 1; i < nH - 1; i++)
    {
      ImgEcrite[i * nW + j] = ImgLue[i * nW + j];
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// remplacer la valeur d'un pixel par la valeur moyenne de ce pixel avec ses 8 voisins (filtre moyenneur)
void filtre_flou_2(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm\n"); 
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel, compteur;
  pixel = compteur = 0;
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
	  {
	    for (int k = i - 1; k < i + 2; k++)
	    {
	      for (int l = j - 1; l < j + 2; l++)
		    {
		      if (k >= 0 && k < nH && l >= 0 && l < nW)
		      {
            pixel += ImgLue[k * nW + l];
            compteur += 1;
		      }
		    }
	    }
      pixel /= compteur;
      ImgEcrite[i * nW + j] = pixel;
      pixel = compteur = 0;
	  }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// remplacer la valeur d'un pixel par la valeur de la moyenne pondérée de ses 8 voisins (filtre gaussien)
void filtre_flou_3(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm\n"); 
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel, compteur;
  pixel = compteur = 0;
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
	  {
	    for (int k = i - 1; k < i + 2; k++)
	    {
	      for (int l = j - 1; l < j + 2; l++)
		    {
		      if (k >= 0 && k < nH && l >= 0 && l < nW)
		      {
            if ((k == i - 1 || k == i + 1) && (l == j - 1 || l == j + 1)) // pixels sur les diagonales
            {
              pixel += ImgLue[k * nW + l];
              compteur += 1;
            }
            else if (k == i && l == j) // pixel du centre
            {
              pixel = pixel + 4 * ImgLue[k * nW + l];
              compteur += 4;
            }
            else // pixel sur les lignes verticales ou horizontales
            {
              pixel = pixel + 2 * ImgLue[k * nW + l];
              compteur += 2;
            }
		      }
		    }
	    }
	  pixel /= compteur;
	  ImgEcrite[i * nW + j] = pixel;
	  pixel = compteur = 0;
	  }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);free(ImgEcrite);
}

// flouter une image couleur
void filtre_flou_couleur(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  if (argc != 3)
  {
    printf("Utilisation: ImgLue.ppm ImgEcrite.ppm \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);

  int pixel_rouge, pixel_vert, pixel_bleu, compteur;
  pixel_rouge = pixel_vert = pixel_bleu = compteur = 0;
  for (int i = 0; i < nH * 3; i += 3)
  {
    for (int j = 0; j < nW * 3; j += 3)
	  {
	    for (int k = i - 3; k < i + 4; k += 3)
	    {
	      for (int l = j - 3; l < j + 4; l += 3)
		    {
		      if (k >= 0 && k < nH * 3 && l >= 0 && l < nW * 3)
		      {
		        pixel_rouge += ImgLue[k * nW + l];
	          pixel_vert += ImgLue[k * nW + l + 1];
	          pixel_bleu += ImgLue[k * nW + l + 2];
		        compteur += 1;
		      }
		    }
	    }
      pixel_rouge /= compteur;
      pixel_vert /= compteur;
      pixel_bleu /= compteur;
      ImgEcrite[i * nW + j] = pixel_rouge;
      ImgEcrite[i * nW + j + 1] = pixel_vert;
      ImgEcrite[i * nW + j + 2] = pixel_bleu;
      pixel_rouge = pixel_vert = pixel_bleu = compteur = 0;
	  }
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite, nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// sauvegarder l'histogramme d'une image pgm
void histogramme(int argc, char* argv[])
{
  char cNomImg[250];
  int nH, nW, nTaille;
  if (argc != 2)
  {
    printf("Utilisation : ImgLue.pgm \n");
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImg);
  OCTET *ImgLue;
  lire_nb_lignes_colonnes_image_pgm(cNomImg, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImg, ImgLue, nH * nW);

  int histogramme[256] = {0};
  int pixel;
  for (int i = 0; i < nTaille; i++)
  {
    pixel = ImgLue[i];
    histogramme[pixel]++;
  }
  std::ofstream f;
  f.open("histogramme.dat", std::ios::out);
  if (f.bad())
  {
    std::cout << "Erreur lors de l'ouverture du fichier" << std::endl;
    exit(1);
  }
  for (int i = 0; i < 256; i++)
  {
    f << i << " " << histogramme[i] << '\n';
  }

  f.close();
  free(ImgLue);
}

// sauvegarder l'histogramme d'une image ppm
void histogramme_couleurs(int argc, char* argv[])
{
  char cNomImgLue[250];
  int nH, nW, nTaille, nR, nG, nB;
  if (argc != 2)
  {
    printf("Utilisation : ImgLue.pgm \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  OCTET *ImgLue;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);

  int histogramme_rouge[256] = {};
  int histogramme_vert[256] = {};
  int histogramme_bleu[256] = {};
  int pixel_rouge;
  int pixel_vert;
  int pixel_bleu;
  for (int i = 0; i < nTaille3; i += 3)
  {
    pixel_rouge = ImgLue[i];
    pixel_vert = ImgLue[i + 1];
    pixel_bleu = ImgLue[i + 2];
    histogramme_rouge[pixel_rouge] += 1;
    histogramme_vert[pixel_vert] += 1;
    histogramme_bleu[pixel_bleu] += 1;
  }
  std::ofstream f;
  f.open("histogramme_couleurs.dat", std::ios::out);
  if (f.bad())
  {  
    std::cout << "Erreur lors de l'ouverture du fichier" << std::endl;
    exit(1);
  }
  for (int i = 0; i < 256; i++)
  {
    f << i << " " << histogramme_rouge[i] << " " << histogramme_vert[i] << " " << histogramme_bleu[i] << "\n";
  }

  free(ImgLue);
}

// copier une image ppm sans la modifier
void indemne(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  if (argc != 3)
  {
    printf("Utilisation: ImgLue.ppm ImgEcrite.ppm \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);

  int pixel_rouge, pixel_vert, pixel_bleu;
  pixel_rouge = pixel_vert = pixel_bleu = 0;
  for (int i = 0; i < nH * 3; i += 3)
  {
    for (int j = 0; j < nW * 3; j += 3)
	  {
      pixel_rouge = ImgLue[i * nW + j];
      pixel_vert = ImgLue[i * nW + j + 1];
      pixel_bleu = ImgLue[i * nW + j + 2];
      ImgEcrite[i * nW + j] = pixel_rouge;
      ImgEcrite[i * nW + j + 1] = pixel_vert;
      ImgEcrite[i * nW + j + 2] = pixel_bleu;
      pixel_rouge = pixel_vert = pixel_bleu = 0;
	  }
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite, nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// inverser les niveaux de gris
void inverse(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm\n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel;
  for (int i = 0; i < nTaille; i++)
  {
    pixel = ImgLue[i];
    ImgEcrite[i] = 255 - pixel;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// à partir de la composante Y.pgm et un paramètre k, génère une image Y dont les niveaux de gris sont modifiés de la valeur k
void modifier_Y(int argc, char* argv[])
{
  if (argc != 4)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm k\n"); 
    exit (1) ;
  }
  if (atoi(argv[3]) < -128 || atoi(argv[3]) > 128)
  {
    printf("k compris strictement entre -128 et 128\n"); 
    exit (1) ;
  }
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  allocation_tableau(ImgEcrite, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);

  int k = atoi(argv[3]);
  int pixel = 0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel = ImgLue[i];
    pixel += k;
    pixel < 0 ? pixel = 0 : pixel > 255 ? pixel = 255 : pixel = pixel;
    ImgEcrite[i] = pixel;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// calculer les gradients horizontal et vertical
void norme_gradient(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm\n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int gradiants_horizontaux[nH][nW];
  int gradiants_verticaux[nH][nW];
  int pixel;
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW - 1; j++)
    {
      pixel = -ImgLue[i * nW + j] + ImgLue[i * nW + j + 1]; 
      gradiants_horizontaux[i][j] = pixel;
    }
    gradiants_horizontaux[i][nW - 1] = 0;
  }
  for (int i = 0; i < nH - 1; i++)
  {
    for (int j = 0; j < nW; j++)
    {
      pixel = -ImgLue[i * nW + j] + ImgLue[(i + 1) * nW + j]; 
      gradiants_verticaux[i][j] = pixel;
    }
  }
  for (int j = 0; j < nW; j++)
  {
    gradiants_verticaux[nH - 1][j] = 0;
  }
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
    {
      ImgEcrite[i * nW + j] = sqrt(gradiants_horizontaux[i][j] * gradiants_horizontaux[i][j] + gradiants_verticaux[i][j] * gradiants_verticaux[i][j]);
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// enchainer une érosion et une dilaration (supprimer des points parasites du fond)
void ouverture(int argc, char* argv[])
{
  if (argc != 3)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm \n");
    exit(1);
  }

  char cNomImgLue[250], cNomImgEcrite[250];
  char cNomTemp[250] = "temporaire.pgm";
  sscanf (argv[1],"%s",cNomImgLue);
  sscanf (argv[2],"%s",cNomImgEcrite);
  argv[2] = cNomTemp;
  erosion(argc, argv);
  argv[1] = cNomTemp;
  argv[2] = cNomImgEcrite;
  dilatation(argc, argv);
  argv[1] = cNomImgLue;

  remove(cNomTemp);
}

// sauvegarde le profil d'une ligne ou d'une colonne d'une image pgm
void profil(int argc, char* argv[])
{
  char cNomImg[250], type[250];
  int nH, nW, nTaille, indice;
  if (argc != 4)
  {
    printf("Utilisation : ImgLue.pgm l|c indice \n");
    exit(1);
  }
  sscanf (argv[1],"%s",cNomImg);
  sscanf (argv[2],"%s",type);
  sscanf (argv[3],"%d",&indice);
  OCTET *ImgLue;
  lire_nb_lignes_colonnes_image_pgm(cNomImg, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImg, ImgLue, nH * nW);

  std::ofstream f;
  f.open("profil.dat", std::ios::out);
  if (f.bad())
  {
    std::cout << "Erreur lors de l'ouverture du fichier" << std::endl;
    exit(1);
  }
  int pixel;
  int taille;
  if (strcmp(type, "c") == 0)
  {
    taille = nH;
  }
  else if (strcmp(type, "l") == 0)
  {
    taille = nW;
  }
  int profil[taille] = {};
  if (strcmp(type, "c") == 0)
  {
    for (int i = 0; i < taille; i++)
    {
      pixel = ImgLue[i * nW + indice];
      profil[i] = pixel;
    }
  }
  else if (strcmp(type, "l") == 0)
  {
    for (int j = 0; j < taille; j++)
    {
      pixel = ImgLue[indice * nW + j];
      profil[j] = pixel;
    }
  }
  for (int i = 0; i < taille; i++)
  {
    f << i << " " << profil[i] << '\n';
  }

  free(ImgLue);
}

// tranformer une image ppm vers une image pgm
void RGB_vers_Y(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB;
  if (argc != 3)
  {
    printf("Utilisation: ImgLue.ppm ImgEcrite.pgm \n"); 
    exit (1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int pixel_rouge, pixel_vert, pixel_bleu ;
  pixel_rouge = pixel_vert = pixel_bleu = 0;
  float pixel_Y = 0.0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel_rouge = ImgLue[i * 3];
    pixel_vert = ImgLue[i * 3 + 1];
    pixel_bleu = ImgLue[i * 3 + 2];
    // pixel_Y = (pixel_rouge + pixel_vert + pixel_bleu) / 3;
    pixel_Y = 0.299 * pixel_rouge + 0.587 * pixel_vert + 0.114 * pixel_bleu;
    ImgEcrite[i] = pixel_Y;
    pixel_rouge = pixel_vert = pixel_bleu = 0;
    pixel_Y = 0.0;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite, nH, nW);
  free(ImgLue);
  free(ImgEcrite);
}

// transformer une image ppm vers 3 composantes Y, Cb et Cr sous la form de 3 images pgm
void RGB_vers_YCbCr(int argc, char* argv[])
{
  if (argc != 5)
  {
    printf("Utilisation: ImgLue.ppm ImgEcriteY.pgm ImgEcriteCb.pgm ImgEcriteCr.pgm \n"); 
    exit (1) ;
  }
  char cNomImgLue[250], cNomImgEcriteY[250], cNomImgEcriteCb[250], cNomImgEcriteCr[250];
  int nH, nW, nTaille, nR, nG, nB;
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcriteY);
  sscanf (argv[3],"%s",cNomImgEcriteCb);
  sscanf (argv[4],"%s",cNomImgEcriteCr);
  OCTET *ImgLue, *ImgEcriteY, *ImgEcriteCb, *ImgEcriteCr;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcriteY, OCTET, nTaille);
  allocation_tableau(ImgEcriteCb, OCTET, nTaille);
  allocation_tableau(ImgEcriteCr, OCTET, nTaille);

  int pixel_rouge, pixel_vert, pixel_bleu;
  pixel_rouge = pixel_vert = pixel_bleu = 0;
  float pixel_Y, pixel_Cb, pixel_Cr;
  pixel_Y = pixel_Cb = pixel_Cr = 0.0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel_rouge = ImgLue[i * 3];
    pixel_vert = ImgLue[i * 3 + 1];
    pixel_bleu = ImgLue[i * 3 + 2];
    pixel_Y = 0.299 * pixel_rouge + 0.587 * pixel_vert + 0.114 * pixel_bleu;
    pixel_Cb = -0.1687 * pixel_rouge - 0.3313 * pixel_vert + 0.5 * pixel_bleu + 128;
    pixel_Cr = 0.5 * pixel_rouge - 0.4187 * pixel_vert - 0.0813 * pixel_bleu + 128;
    ImgEcriteY[i] = pixel_Y;
    ImgEcriteCb[i] = pixel_Cb;
    ImgEcriteCr[i] = pixel_Cr;
    pixel_rouge = pixel_vert = pixel_bleu = 0;
    pixel_Y = pixel_Cb = pixel_Cr = 0.0;
  }

  ecrire_image_pgm(cNomImgEcriteY, ImgEcriteY, nH, nW);
  ecrire_image_pgm(cNomImgEcriteCb, ImgEcriteCb, nH, nW);
  ecrire_image_pgm(cNomImgEcriteCr, ImgEcriteCr, nH, nW);
  free(ImgLue);
  free(ImgEcriteY);
  free(ImgEcriteCb);
  free(ImgEcriteCr);
}

// seuiller une image ppm en 2 parties
void seuil_couleur(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, S;
  if (argc != 4) 
  {
    printf("Utilisation : ImgLue.ppm ImgEcrite.ppm Seuil \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);
	
  for (int i=0; i < nTaille3; i+=3)
  {
    nR = ImgLue[i];
    nG = ImgLue[i+1];
    nB = ImgLue[i+2];
    if (nR < S) ImgEcrite[i]=0; else ImgEcrite[i]=255;
    if (nG < S) ImgEcrite[i+1]=0; else ImgEcrite[i+1]=255; 
    if (nB < S) ImgEcrite[i+2]=0; else ImgEcrite[i+2]=255;
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);
}

// seuiller séparément chaque composante d'une image ppm en 2 parties
void seuil_couleur_composante(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nR, nG, nB, S_R, S_V, S_B;
  if (argc != 6)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm S_R S_V S_S_B \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S_R);
  sscanf (argv[4],"%d",&S_V);
  sscanf (argv[5],"%d",&S_B);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLue, OCTET, nTaille3);
  lire_image_ppm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);

  for (int i = 0; i < nTaille3; i += 3)
  {
    nR = ImgLue[i];
    nG = ImgLue[i + 1];
    nB = ImgLue[i + 2];
    if (nR < S_R) ImgEcrite[i] = 0; else ImgEcrite[i] = 255;
    if (nG < S_V) ImgEcrite[i + 1] = 0; else ImgEcrite[i + 1] = 255; 
    if (nB < S_B) ImgEcrite[i + 2] = 0; else ImgEcrite[i + 2] = 255;
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue);free(ImgEcrite);
}

// seuiller une image pgm en 2 parties
void seuil_gris_2(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  if (argc != 4) 
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm Seuil \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
    {
      if (ImgLue[i*nW+j] < S) ImgEcrite[i*nW+j]=0; else ImgEcrite[i*nW+j]=255;
    }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue); free(ImgEcrite);
}

// seuiller une image pgm en 2 parties afin que le fond soit en blanc
void seuil_gris_fond_blanc(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S;
  if (argc != 4) 
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm Seuil \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
    {
      if (ImgLue[i*nW+j] < S) ImgEcrite[i*nW+j]=0; else ImgEcrite[i*nW+j]=255;
    }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue); free(ImgEcrite);
}

// seuiller une image pgm en 3 parties
void seuil_gris_3(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S1, S2;
  if (argc != 5)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm Seuil1 Seuil2 \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S1);
  sscanf (argv[4],"%d",&S2);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  for (int i = 0; i < nTaille; i++)
  {
    if (ImgLue[i] < S1) ImgEcrite[i] = 0;
    else if (ImgLue[i] < S2) ImgEcrite[i] = 128;
    else ImgEcrite[i] = 255;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue); free(ImgEcrite);
}

// seuiller une image pgm en 4 parties
void seuil_gris_4(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S1, S2, S3;
  if (argc != 6)
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm Seuil1 Seuil2 Seuil3 \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&S1);
  sscanf (argv[4],"%d",&S2);
  sscanf (argv[5],"%d",&S3);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);
  for (int i = 0; i < nTaille; i++)
  {
    if (ImgLue[i] < S1) ImgEcrite[i] = 0;
    else if (ImgLue[i] < S2) ImgEcrite[i] = 64;
    else if (ImgLue[i] < S3) ImgEcrite[i] = 128;
    else ImgEcrite[i] = 255;
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue); free(ImgEcrite);
}

// seuiller une image avec un seuil bas et un seuil haut en deux phases
void seuil_hysteresis(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SB, SH;
  if (argc != 5) 
  {
    printf("Utilisation : ImgLue.pgm ImgEcrite.pgm SeuilBas SeuilHaut \n"); 
    exit(1) ;
  }
  sscanf (argv[1],"%s",cNomImgLue) ;
  sscanf (argv[2],"%s",cNomImgEcrite);
  sscanf (argv[3],"%d",&SB);
  sscanf (argv[4],"%d",&SH);
  OCTET *ImgLue, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
  nTaille = nH * nW;
  allocation_tableau(ImgLue, OCTET, nTaille);
  lire_image_pgm(cNomImgLue, ImgLue, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille);

  int image_pre_seuillee[nTaille];
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
    {
      if (ImgLue[i * nW + j] <= SB)
      {
        image_pre_seuillee[i * nW + j] = 0;
      }
      else if (ImgLue[i * nW + j] >= SH)
      {
        image_pre_seuillee[i * nW + j] = 255;
      }
      else
      {
        image_pre_seuillee[i * nW + j] = ImgLue[i * nW + j];
        ImgEcrite[i * nW + j] = ImgLue[i * nW + j];
      }
    }
  }
  int pixel;
  for (int i = 0; i < nH; i++)
  {
    for (int j = 0; j < nW; j++)
    {
      if (image_pre_seuillee[i * nW + j] > SB && image_pre_seuillee[i * nW + j] < SH)
      {
        pixel = 0;
        for (int k = i - 1; k < i + 2; k++)
        {
          for (int l = j - 1; l < j + 2; l++)
            {
              if (k >= 0 && k < nH && l >= 0 && l < nW && image_pre_seuillee[k * nW + l] == 255)
              {
                pixel = 255;
              }
            }
        }
        ImgEcrite[i * nW + j] = pixel;
      }
    }
  }

  ecrire_image_pgm(cNomImgEcrite, ImgEcrite,  nH, nW);
  free(ImgLue); free(ImgEcrite);
}

// transformer 3 composantes Y, Cr et Cb vers une image couleur ppm
void YCbCr(int argc, char* argv[])
{
  if (argc != 5)
  {
    printf("Utilisation : ImgLueY.pgm ImgLueCb.pgm ImgLueCr.pgm ImgEcrite.ppm \n"); 
    exit (1) ;
  }
  char cNomImgLueY[250], cNomImgLueCb[250], cNomImgLueCr[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  sscanf (argv[1],"%s",cNomImgLueY) ;
  sscanf (argv[2],"%s",cNomImgLueCb);
  sscanf (argv[3],"%s",cNomImgLueCr);
  sscanf (argv[4],"%s",cNomImgEcrite);
  OCTET *ImgLueY, *ImgLueCb, *ImgLueCr, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLueY, OCTET, nTaille);
  lire_image_pgm(cNomImgLueY, ImgLueY, nH * nW);
  allocation_tableau(ImgLueCb, OCTET, nTaille);
  lire_image_pgm(cNomImgLueCb, ImgLueCb, nH * nW);
  allocation_tableau(ImgLueCr, OCTET, nTaille);
  lire_image_pgm(cNomImgLueCr, ImgLueCr, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);

  int pixel_rouge, pixel_vert, pixel_bleu, pixel_Y, pixel_Cb, pixel_Cr;
  pixel_rouge = pixel_vert = pixel_bleu = pixel_Y = pixel_Cb = pixel_Cr = 0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel_Y = ImgLueY[i];
    pixel_Cb = ImgLueCb[i];
    pixel_Cr = ImgLueCr[i];
    pixel_rouge = pixel_Y + 1.402 * (pixel_Cr - 128);
    pixel_vert = pixel_Y - 0.34414 * (pixel_Cb - 128) - 0.714414 * (pixel_Cr - 128);
    pixel_bleu = pixel_Y + 1.772 * (pixel_Cb - 128);
    pixel_rouge < 0 ? pixel_rouge = 0 : pixel_rouge > 255 ? pixel_rouge = 255 : pixel_rouge = pixel_rouge;
    pixel_vert < 0 ? pixel_vert = 0 : pixel_vert > 255 ? pixel_vert = 255 : pixel_vert = pixel_vert;
    pixel_bleu < 0 ? pixel_bleu = 0 : pixel_bleu > 255 ? pixel_bleu = 255 : pixel_bleu = pixel_bleu;
    ImgEcrite[i * 3] = pixel_rouge;
    ImgEcrite[i * 3 + 1] = pixel_vert;
    ImgEcrite[i * 3 + 2] = pixel_bleu;
    pixel_rouge = pixel_vert = pixel_bleu = pixel_Y = pixel_Cb = pixel_Cr = 0;
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite, nH, nW);
  free(ImgLueY);
  free(ImgLueCb);
  free(ImgLueCr);
  free(ImgEcrite);
}

// transformer 3 composantes Y, Cr et Cb vers une image couleur RBG
void YCbCr_vers_RBG(int argc, char* argv[])
{
  if (argc != 5)
  {
    printf("Utilisation : ImgLueY.pgm ImgLueCb.pgm ImgLueCr.pgm ImgEcrite.ppm \n"); 
    exit (1) ;
  }
  char cNomImgLueY[250], cNomImgLueCb[250], cNomImgLueCr[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  sscanf (argv[1],"%s",cNomImgLueY) ;
  sscanf (argv[2],"%s",cNomImgLueCb);
  sscanf (argv[3],"%s",cNomImgLueCr);
  sscanf (argv[4],"%s",cNomImgEcrite);
  OCTET *ImgLueY, *ImgLueCb, *ImgLueCr, *ImgEcrite;
  lire_nb_lignes_colonnes_image_pgm(cNomImgLueY, &nH, &nW);
  nTaille = nH * nW;
  int nTaille3 = nTaille * 3;
  allocation_tableau(ImgLueY, OCTET, nTaille);
  lire_image_pgm(cNomImgLueY, ImgLueY, nH * nW);
  allocation_tableau(ImgLueCb, OCTET, nTaille);
  lire_image_pgm(cNomImgLueCb, ImgLueCb, nH * nW);
  allocation_tableau(ImgLueCr, OCTET, nTaille);
  lire_image_pgm(cNomImgLueCr, ImgLueCr, nH * nW);
  allocation_tableau(ImgEcrite, OCTET, nTaille3);

  int pixel_rouge, pixel_vert, pixel_bleu, pixel_Y, pixel_Cb, pixel_Cr;
  pixel_rouge = pixel_vert = pixel_bleu = pixel_Y = pixel_Cb = pixel_Cr = 0;
  for (int i = 0; i < nTaille; i++)
  {
    pixel_Y = ImgLueY[i];
    pixel_Cb = ImgLueCb[i];
    pixel_Cr = ImgLueCr[i];
    pixel_rouge = pixel_Y + 1.402 * (pixel_Cr - 128);
    pixel_vert = pixel_Y - 0.34414 * (pixel_Cb - 128) - 0.714414 * (pixel_Cr - 128);
    pixel_bleu = pixel_Y + 1.772 * (pixel_Cb - 128);
    pixel_rouge < 0 ? pixel_rouge = 0 : pixel_rouge > 255 ? pixel_rouge = 255 : pixel_rouge = pixel_rouge;
    pixel_vert < 0 ? pixel_vert = 0 : pixel_vert > 255 ? pixel_vert = 255 : pixel_vert = pixel_vert;
    pixel_bleu < 0 ? pixel_bleu = 0 : pixel_bleu > 255 ? pixel_bleu = 255 : pixel_bleu = pixel_bleu;
    ImgEcrite[i * 3] = pixel_rouge;
    ImgEcrite[i * 3 + 1] = pixel_bleu;
    ImgEcrite[i * 3 + 2] = pixel_vert;
    pixel_rouge = pixel_vert = pixel_bleu = pixel_Y = pixel_Cb = pixel_Cr = 0;
  }

  ecrire_image_ppm(cNomImgEcrite, ImgEcrite, nH, nW);
  free(ImgLueY);
  free(ImgLueCb);
  free(ImgLueCr);
  free(ImgEcrite);
}
