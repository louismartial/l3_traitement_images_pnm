# Manipuler et traiter des images *pgm* et *ppm* à partir d'une librairie qui ouvre et écrit des images *pgm* et *ppm*

## *difference*

Visualise les contours des objets contenus dans l’image *pgm* :

* Si les deux pixels (de l’image seuillée et de l’image dilatée) appartiennent au fond, alors le pixel correspondant de l’image de sortie appartient au fond (255).
* Si les deux pixels (de l’image seuillée et de l’image dilatée) appartiennent à l’objet, alors le pixel correspondant de l’image de sortie appartient au fond (255).
* Sinon, le pixel correspondant de l’image de sortie appartient au contour (0).

## *dilatation*

Bouche des petits trous blancs isolés dans les objets objets d'une image *pgm*, en transformant les pixels blancs d'un objet en pixels de cet objet. Pour chaque pixel, remplace dans l'image de destination sa valeur d'origine par la valeur minimale de ses pixels voisins.

## *erreur_quadratique_moyenne*

Calcule l'erreur quadratique moyenne entre une image *ppm* et sa version *pgm*.

## *erosion*

Supprime les points objets noirs isolés d'une image *pgm*, en transformant les pixels noirs du fond en pixels du fond. Pour chaque pixel, remplace dans l'image de destination sa valeur d'origine par la valeur maximale de ses pixels voisins.

## *fermeture*

Enchaine une dilatation et une érosion afin de boucher des petits trous isolés dans les objets contenus dans l’image *pgm*.

## *filtre_flou_1*

Remplace, dans l’image *pgm* de sortie, la valeur d’un pixel par la valeur moyenne de ce pixel avec ses 4 voisins. Conserve les valeurs initiales de la première et de la dernière colonne, ainsi que la première et la dernière ligne.

## *filtre_flou_2*

Remplacer, dans l'image *pgm* de sortie, la valeur d'un pixel par la valeur moyenne de ce pixel avec ses 8 voisins.

## *filtre_flou_couleur*

Floute une image *ppm* dans sa globalité. Fait séparément sur chacune des composantes de l’image, chaque pixel de l'image est remplacé par la valeur moyenne calculée avec ses voisins.

## *histogramme*

Sauvegarde dans un fichier les données de l'histogramme d’une image *pgm*. Le fichier contient 2 colonnes : indice et occurrence des niveaux de gris.

## *histograme_couleurs*

Sauvegarde dans un fichier, les données des histogrammes des trois composantes couleur (Rouge, Vert, Bleu) d'une image *ppm*. Le fichier contient 4 colonnes : indice (de 0 à 255) et occurrence du rouge, puis du vert et enfin du bleu.

## *inverse*

Inverse les niveaux de gris d’une image *pgm*. Le noir (0) devient blanc (255), et le blanc devient noir, ainsi que tous les niveaux de gris qui sont inversés.

## *modifier_Y*

À partir de la composante Y d'une image *pgm* et un paramètre k (avec -128 < k < +128) génère une nouvelle image de luminance Y modifiée dont tous les niveaux de gris sont modifiés de la valeur k.

## *norme_gradient*

En chaque point d'une image *pgm* calcule les gradients horizontal et vertical, puis retourne la norme du gradient. Crée alors une image de la norme des gradients.

## *ouverture*

Enchaine une érosion et une dilatation afin de supprimer des points parasites du fond de l’image *pgm*.

## *profil*

Sauvegarde dans un fichier sur 2 colonnes les indices et les niveaux de gris d'une ligne ou d'une colonne d'une image *pgm*. Prend comme arguments, le nom l’image, une information précisant s'il s'agit d'une ligne ou d'une colonne, et un indice indiquant le numéro de la ligne ou de la colonne.

## *RGB_vers_Y*

Transforme une image *ppm* en une image en niveau de gris *pgm*.

## *RGB_vers_YCbCr*

Prend en entrée une image couleur *ppm* et fournit en sortie les 3 composantes Y, Cb et Cr sous la forme de 3 images *pgm*.

## *seuil_couleur_composante*

Seuille séparément chacune des trois composantes couleurs (Rouge, Vert, Bleu) d'une image *ppm*. 3 paramètres S_R, S_G, S_S_B sont nécessaires.

## *seuil_gris_2*

Seuille une image *pgm* en 2 parties :

* Si p_in(i,j) < S, alors p_out(i,j) = 0 (noir)
* Sinon, p_out(i,j) = 255 (blanc)


## *seuil_gris_3*

Seuille une image *pgm* en 3 parties :

* Si p_in(i,j) < S1, alors p_out(i,j) = 0
* Sinon si p_in(i,j) < S2, alors p_out(i,j) = 128 (gris)
* Sinon, p_out(i,j) = 255

## *seuil_gris_4*

Seuille une image *pgm* en 4 parties :

* Si p_in(i,j) < S1, alors p_out(i,j) = 0
* Sinon si p_in(i,j) < S2, alors p_out(i,j) = 64 (gris clair)
* Sinon si p_in(i,j) < S3, alors p_out(i,j) = 128 (gris)
* Sinon, p_out(i,j) = 255

## *seuil_hysteresis*

S'appuie sur un seuil bas SB et un seuil haut SH.

* À la première lecture de l’image *pgm* de la norme des gradients, si norme du gradient <= SB alors 0, si norme du gradient >= SH alors 255.
* À la seconde lecture de l’image de la norme de gradients pré-seuillée avec SB et SH, si SB < norme du gradient < SH et qu’au moins 1 de ses voisins = 255 alors 255 sinon 0.

## *YCbCr*

À partir des trois composantes Y, Cb et Cr, reconstruit une image couleur *ppm*.

## *YCbCr_vers_RBG*

À partir des trois composantes Y, Cb et Cr, reconstruit une image couleur *ppm*, mais en inversant la compsante verte et la composante bleue.